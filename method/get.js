const { isMsg } = require('ssb-ref')

module.exports = function Get (server, loadCrut, type) {
  return function get (id, cb) {
    if (!isMsg(id)) return cb(new Error(`${type}.get requires a %msgId`))

    loadCrut(id, (err, crut) => {
      if (err) return cb(err)

      crut.read(id, (err, data) => {
        if (err) return cb(err)

        data.states = data.states.map(state => {
          return Object.assign(state, {
            type: crut.spec.type.replace(`${type}/`, '')
            // decorate with sub-type
          })
        })

        cb(null, data)
      })
    })
  }
}
