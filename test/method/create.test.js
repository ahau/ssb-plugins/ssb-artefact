const test = require('tape')
const Server = require('../test-bot')
const { isMsgId } = require('ssb-ref')

test('artefact.create', async t => {
  t.plan(16)
  async function runCreateTest (type, artefactBlob, details, expectedErr = null, msg) {
    const server = await Server()

    details.authors = {
      add: ['*']
    }

    if (details.recps) {
      details.recps = [server.id]
    }

    server.artefact.create(type, artefactBlob, details, (err, artefactId) => {
      if (expectedErr) {
        t.deepEqual(err, expectedErr, msg)
        server.close()
        return
      }

      if (err) {
        console.error('Publishing problem', type, artefactBlob, details)
        throw err
      }

      t.true(isMsgId(artefactId), msg || 'returns a valid artefact id')
      server.close()
    })
  }

  const blobId = '&1ZQM7TjQHBUEcdBijB6y7dkX047wCf4aXcjFplTjrJo=.sha256'
  const unbox = 'YmNz1XfPw/xkjoN594ZfE/JUhpYiGyOOQwNDf6DN+54=.boxs'

  const artefactBlob = {
    type: 'ssb',
    blobId,
    mimeType: 'application/pdf',
    size: 12345,
    unbox
  }

  const artefactHyperBlob = {
    type: 'hyper',
    driveAddress: '6LJVf5C5T1WauuJUgCWUA0mT+ak9pV5fFH4uhw2PeVU=',
    blobId: '2df0cb34-77e5-40af-99ad-52d85ff67d35',
    readKey: 'Wenusia9d1gdhJNGsgY+66xRjgbl82N7daFeaM436qs=',
    mimeType: 'video/mp4',
    size: 12345
  }

  const tombstone = {
    date: Date.now(), // => big integer
    reason: 'duplicate'
  }

  const audioVideoProperties = {
    duration: 123456,
    transcription: 'some transcription'
  }

  const allProperties = {
    title: 'Hello Audio',
    description: 'this is an audio',
    createdAt: '2020-07-13',
    identifier: 'AF123456',
    language: 'English',
    licence: 'Attribution CC BY',
    rights: 'All rights belong to the Author Cherese Eriepa. Phone 0212345678',
    source: 'www.ahau.io',
    translation: 'Kia ora',
    location: 'Raglan'
  }

  // video
  await runCreateTest(
    'video',
    artefactBlob,
    {
      ...audioVideoProperties,
      ...allProperties
    },
    null,
    'accept video input'
  )
  // video with hyper-blob
  await runCreateTest(
    'video',
    artefactHyperBlob,
    {
      ...audioVideoProperties,
      ...allProperties
    },
    null,
    'accept video input (hyper-blobs)'
  )

  await runCreateTest(
    'video',
    artefactBlob,
    {
      tombstone
    },
    null,
    'tombstone gets removed'
  )

  // audio
  await runCreateTest(
    'audio',
    artefactBlob,
    {
      ...audioVideoProperties,
      ...allProperties
    },
    null,
    'accept audio input'
  )

  await runCreateTest(
    'audio',
    artefactBlob,
    {
      tombstone
    },
    null,
    'tombstone gets removed'
  )

  // photo
  await runCreateTest(
    'photo',
    artefactBlob,
    {
      ...allProperties
    },
    null,
    'accept photo input'
  )

  await runCreateTest(
    'photo',
    artefactBlob,
    {
      tombstone
    },
    null,
    'tombstone gets removed'
  )

  // photo doesnt accept video/audio properties
  await runCreateTest(
    'photo',
    artefactBlob,
    {
      ...audioVideoProperties,
      ...allProperties
    },
    new Error('video/audio properties not allowed for type photo'),
    'photo doesnt accept video/audio properties'
  )

  // type is null
  await runCreateTest(null, artefactBlob, {}, new Error('invalid artefact type'), 'dont accept null type')

  // type is invalid type
  await runCreateTest('somethingElse', artefactBlob, {}, new Error('invalid artefact type'), 'dont accept invalid type')

  // type is invalid integer type
  await runCreateTest(123456, artefactBlob, {}, new Error('invalid artefact type'), 'dont accept invalid type (value cant be an integer)')

  // artefactBlob is null (photo)
  await runCreateTest('photo', null, {}, new Error('data.blob referenced schema does not match'), 'dont accept null blob (photo)')

  // artefactBlob is undefined (video)
  await runCreateTest('video', undefined, {}, new Error('invalid artefact blob'), 'dont accept undefined blob')

  // blob is null (video)
  await runCreateTest('video', null, {}, new Error('data.blob referenced schema does not match'), 'dont accept null blob (video)')

  // blob is null (audio)
  await runCreateTest('audio', null, {}, new Error('data.blob referenced schema does not match'), 'dont accept null blob (audio)')

  // invalid blob
  await runCreateTest('audio', 'someInvalidBlob', {}, new Error('data.blob referenced schema does not match'), 'dont accept null blob (audio)')
})
