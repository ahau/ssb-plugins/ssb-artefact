# ssb-artefact

An `ssb-server` plugin for creating, reading and updating artefacts in `scuttlebutt`

## Modules

* ssb-artefact

---

## Artefact

An artefact is a media file that is stored using ssb.

```js
   A   (the root message)
   |
   B   (an edit after A)
  / \
 C   D (two concurrent edits after B)
```

Because there might be multiple offline edits to an artefact which didn't know bout one-another, it's possible for divergence to happen:
`C` and `D` wont know about each other.

An artefact is an Object which maps the key of each latest edit to the state it perceives the artefact to be in:

```js
// artefact for the example above
{
  key: MessageId, // artefactId supplied, the root message of the artefact
  states: [
    { head: C, state: stateC },
    { head: D, state: stateD },
  ]
}
```

and the `state` is an Object which looks like the following for the four different artefact types:

```js
{
  // fields for all types
  type: String,
  blob: Blob,
  createdAt: EdtfDateString

  title: String,
  description: String,

  identifier: String,
  licence: String,
  rights: String,
  source: String,

  language: String,
  translation: String

  authors: {
    [FeedId]: [
      { start: Integer, end: Integer }
    ]
  }

  recps: Recps,
  tombstone: Tombstone

  // additional fields for video and audio types only
  duration: Integer
  transcription: String
}
```

---

## API

### `server.artefact.create(type, artefactBlob, details, cb)`

Creates a new artefact record and describes details you'd like to set on it

* `type` *String* - describes the type of artefact
  * Only accepts `photo`, `video` or `audio`
* `artefactBlob` *Blob* - the blob object for the artefact media
* `details` *Object* - allows you to specify additional fields depending on the type of artefact. See example below
* `cb` *Function* - a callback with signature (err, artefactId)

The expected form of `details`:

```js
{
  // fields for all types
  recps: [ FeedId, ...],           // can only be set in create

  authors: {
    add: [FeedId, ALL_AUTHORS],
    remove: [FeedId, ALL_AUTHORS] // NOTE: not available on create
  }

  title: { set: String },
  description: { set: String },

  createdAt: { set: EdtfDateString },

  identifier: { set: String },
  licence: { set: String },
  rights: { set: String },
  source: { set: String },

  language: { set: String },
  translation: { set: String },

  tombstone: { set: Tombstone },

  // additional fields for video and audio types only
  duration: { set: Integer },
  transcription: { set: String }
}
```

* type `Tombstone` is either `null` OR an Object of shape:

  ```js
  {
    date: UnixTime,  // an Integer indicating microseconds from 1st Jan 1970, can be negative!
    reason: String   // (optional)
  }
  ```

* `recps` is a special scuttlebutt property, short for "recipients". Adding this means the artefact will automatically be encrypted so only the `FeedId` and `GroupId` listed in the `recps` Array will be able to read this artefact.

* type `EdtfDateString` is an edtf date string field. See [edtf](https://github.com/inukshuk/edtf.js) for more details.
* `authors` contains two arrays (add, remove) for adding and removing authors to the set. Authors are of the form `feedId` or `*`:
  * `FeedId` - updates from this author will be valid
  * `*` - updates by all authors will be valid
  * NOTE: authors are valid until they are removed from the set. When this feedId, it overrides all authors until it is removed
  * Any updates that arent from a valid author are classed as invalid and wont be returned when using the get method

> Note: All of these fields are optional

---

### `server.artefact.get(artefactId, cb)`

Gets an artefact record by its `artefactId`

* `artefactId` *String* - the cypherlink for the artefact (the `msgId` of the root message for the artefact)
* `cb` *Function* - a callback with signature `(err, artefact)`

All fields will be returned, but if no value has been set/added for that field, the value will be `null`

---

### `server.artefact.update(artefactId, details, cb)`

Gets an artefact record by its `artefactId`

* `artefactId` *String* - the cypherlink for the artefact (the `msgId` of the root message for the artefact)
* `details` *Object* - same as in `artefact.create` except recps is set for you based on what the first message was
* `cb` *Function* - a callback with signature `(err)`

All fields will be returned, but if no value has been set/added for that field, the value will be `null`

---
